const items=[
    {
        "id":1,
        "name":"Dalgona coffee (1kg)",
        "cost":700
    },
    {
        "id":2,
        "name":"Dalgona coffee (1/2kg)",
        "cost":350,
    },
    {
        "id":3,
        "name":"Black forest (1kg)",
        "cost":550,
    },
    {
        "id":4,
        "name":"Black forest (1/2kg)",
        "cost":300,
    },
    {
        "id":5,
        "name":"White forest (1kg)",
        "cost":550,
    },
    {
        "id":6,
        "name":"White forest (1/2kg)",
        "cost":300
    },
    {
        "id":7,
        "name":"Chocolate oreo cake (1kg)",
        "cost":700,
    },
    {
        "id":8,
        "name":"Chocolate oreo (1/2kg)",
        "cost":350,
    },
    {
        "id":9,
        "name":"Chocolate turffel (1kg)",
        "cost":800,
    },
    {
        "id":10,
        "name":"Chocolate turffel (1/2kg)",
        "cost":400,
    },
    {
        "id":11,
        "name":"Pinapple delite (1kg)",
        "cost":500,
    },
    {
        "id":12,
        "name":"Pinapple delite (1/2kg)",
        "cost":250,
    },
    {
        "id":13,
        "name":"Strawberry delite (1kg)",
        "cost":500,
    },
    {
         "id":14,
         "name":"Strawberry delite (1/2kg)",
         "cost":250,
    },
    {
        "id":15,
        "name":"Blueberry (1kg)",
        "cost":500,
    },
    {
        "id":16,
        "name":"Blueberry (1/2kg)",
        "cost":250,
    },
    {
        "id":17,
        "name":"Mango delite (1kg)",
        "cost":500,
    },
    {
        "id":18,
        "name":"Mango delite (1/2kg)",
        "cost":250,
    },
    {
        "id":19,
        "name":"Red velvet (1kg)",
        "cost":550,
    },
    {
        "id":20,
        "name":"Butterscotch (1kg)",
        "cost":500,
    },
    {
        "id":21,
        "name":"Royal Motichoor (1kg)",
        "cost":800,
    },
    {
        "id":22,
        "name":"Royal Kala Jamun (1kg)",
        "cost":850,
    },
    {
        "id":23,
        "name":"Kulfi Faluda (1kg)",
        "cost":550,
    },
    {
        "id":24,
        "name":"Royal Rasmalai (1kg)",
        "cost":800,
    },
    {
        "id":25,
        "name":"Rose Gulkand (1kg)",
        "cost":550,
    }
];
var totalamount=0;
var itemcount=0;
function addTocart(id)
{
    var itemid = id;
    var item=items.find(item => item.id ===itemid);
    console.log(item);
    var itemcost= item.cost;
    var itemname= item.name;
    var quantity=1;
    itemcount+=1;
    totalamount+=quantity*itemcost;

    var cart= document.getElementById("carttabel");
    cart.innerHTML+= 
    `
    <tr>
      <th>${itemcount}</th>
      <th>${itemname}</th>
      <th>${quantity}</th>
      <th>${itemcost}</th>
      <th>${quantity*itemcost}</th>
    </tr>
    `
    var carttotal=document.getElementById("cartTotal");
    carttotal.innerHTML+=
    `
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th><b> Total = <?b></th>
      <th>${totalamount}</th>
    </tr>
    `
}